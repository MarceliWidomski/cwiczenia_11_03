//============================================================================
// Name        : ilosc_banknotow.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout
			<< "Program wypisuje minimalna ilosc banknotow o danych nominalach potrzebna do wyplaty kwoty"
			<< endl;
	cout << "Podaj kwote: ";
	int kwota;
	cin >> kwota;
	if (kwota % 10 != 0) // program nie dokonuje obliczen jesli podana kwota nie jest wielokrotnoscia 10 zl
		cout
				<< "Podana kwota musi byc wielokrotnoscia banknotu o najmniejszym nominale (10 zl)"
				<< endl;
	else {
		int pozostalo(kwota);
		int ilosc200(0);
		while (pozostalo >= 200) {
			pozostalo -= 200;
			++ilosc200;
		}
		int ilosc100(0);
		while (pozostalo >= 100) {
			pozostalo -= 100;
			++ilosc100;
		}
		int ilosc50(0);
		while (pozostalo >= 50) {
			pozostalo -= 50;
			++ilosc50;
		}
		int ilosc20(0);
		while (pozostalo >= 20) {
			pozostalo -= 20;
			++ilosc20;
		}
		int ilosc10(0);
		while (pozostalo >= 10) {
			pozostalo -= 10;
			++ilosc10;
		}
		cout << "Do wyplacenie kwoty " << kwota
				<< " potrzebne sa nastepujace nominaly: " << endl;
		cout << ilosc200 << " x 200 zl" << endl;
		cout << ilosc100 << " x 100 zl" << endl;
		cout << ilosc50 << " x 50 zl" << endl;
		cout << ilosc20 << " x 20 zl" << endl;
		cout << ilosc10 << " x 10 zl" << endl;
	}
	return 0;
}
