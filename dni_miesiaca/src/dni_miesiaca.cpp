//============================================================================
// Name        : dni_miesiaca.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	unsigned short opcja(0);
	cout << "Program informuje ile dni ma podany miesiac" << endl;
	while (opcja != 2) {
		cout << "1) Wprowadz miesiac" << endl;
		cout << "2) Zakoncz" << endl;
		cin >> opcja;
		if (opcja == 1) {
			cout << "Podaj numer miesiaca: ";
			unsigned short miesiac;
			cin >> miesiac;
			if (miesiac == 1 || miesiac == 3 || miesiac == 5 || miesiac == 7
					|| miesiac == 8 || miesiac == 10 || miesiac == 12)
				cout << "Podany miesiac ma 31 dni" << endl;
			else if (miesiac == 4 || miesiac == 6 || miesiac == 9
					|| miesiac == 11)
				cout << "Podany miesiac ma 30 dni" << endl;
			else if (miesiac == 2) {
				cout
						<< "Podany miesiac ma 28 lub 29 dni, w zaleznosci czy rok jest przestepny."
						<< endl;
				cout << "Czy chcesz sprawdzic czy rok jest przestepny?" << endl;
				cout << "1) Tak" << endl;
				cout << "2) Nie" << endl;
				unsigned short opcja2;
				cin >> opcja2;
				if (opcja2 == 1) {
					cout << "Podaj rok: ";
					int rok;
					cin >> rok;
					if ((rok % 4 == 0 && rok % 100 != 0) || rok % 400 == 0) // sprawdza czy rok jest przestepny
						cout << "Rok jest przestepny, miesiac ma 29 dni"
								<< endl;
					else
						cout << "Rok nie jest przestepny, miesiac ma 28 dni"
								<< endl;
				}
			} else {
				cout << "Nielwasciwy numer miesiaca." << endl;
				if (miesiac == 0){ // przerywa program gdy w miejsce miesiaca zostanie podana wartosc nie odpowiadajaca typo unsigned short
					cout << "Zly format miesiaca. Awaryjne wylaczenie programu..." << endl;
					break;
					break;
				}
			}
		} else if (opcja == 2)
			cout << "Wylaczam..." << endl;
		else {
			cout << "Niewlasciwy wybor opcji" << endl;
			if (opcja == 0) {
				cout << "Zly format opcji. Awaryjne wylaczenie programu..." << endl; // przerywa gdy opcja ma niewlasciwy format
				break;
			}
		}
	}
	return 0;
}
