//============================================================================
// Name        : ilosc_cyfr_liczby.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program zlicza ilosc cyfr w podanej liczbie" << endl;
	int liczba;
	cout << "Podaj liczbe: ";
	cin >> liczba;
	int iloscCyfr(0);
	int tymczasowa(liczba);
	while (tymczasowa != 0) {
		++iloscCyfr;
		tymczasowa /= 10; // "usuwa" cyfre jednosci z liczby
	}
	cout << "Podana liczba sklada sie z " << iloscCyfr << " cyfr";
	return 0;
}
