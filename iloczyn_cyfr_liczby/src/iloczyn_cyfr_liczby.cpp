//============================================================================
// Name        : iloczyn_cyfr_liczby.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program zlicza iloczyn cyfr podanej liczby" << endl;
	cout << "Wprowadz liczbe: ";
	int liczba;
	cin >> liczba;
	int wynik(1);
	while (liczba != 0) {
		int ostatnia(liczba % 10);
		liczba /= 10;
		wynik *= ostatnia;
	}
	cout << "Iloczyn cyft podanej liczby wynosi: " << wynik << endl;
	return 0;
}
