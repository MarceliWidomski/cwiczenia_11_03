//============================================================================
// Name        : rodzaj_znaku.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	unsigned short opcja(0);
	while (opcja != 2) {
		cout << "Program sprawdza jakiego rodzaju znak zostal wprowadzony"
				<< endl;

		cout << "1) Wprowadz znak" << endl;
		cout << "2) Zakoncz" << endl;
		cin >> opcja;
		if (opcja == 1) {
			cout << "Wprowadz znak: ";
			char znak;
			cin >> znak;
			if (znak >= 97 && znak <= 122)
				cout << "Wprowadziłes znak \"" << znak
						<< "\" ktory jest mala litera alfabetu" << endl;
			else if (znak >= 65 && znak <= 90)
				cout << "Wprowadziłes znak \"" << znak
						<< "\" ktory jest wielka litera alfabetu" << endl;
			else if (znak >= 48 && znak <= 57)
				cout << "Wprowadziłes znak \"" << znak << "\" ktory jest cyfra"
						<< endl;
			else
				cout << "Wprowadziłes znak \"" << znak
						<< "\" ktory jest znakiem specjalnym" << endl;
		} else if (opcja == 2)
			cout << "Wylaczam..." << endl;
		else { // pozwala wybrac ponownie opcje w przypadku wpisania zlej liczby
			cout << "Niewlasciwy wybor opcji" << endl;
			if (opcja == 0) { // awaryjnie wylacza program jesli w miejsce opcji zostanie wstawiona wartosc nieobslugiwana przez typ short
				cout << "Wylaczam program...";
				break;
			}
		}
	}
	return 0;
}
