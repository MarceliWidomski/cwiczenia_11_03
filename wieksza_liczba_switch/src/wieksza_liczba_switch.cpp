//============================================================================
// Name        : wieksza_liczba_switch.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//program sprawdza ktora z liczb jest wieksza z wykorzystaniem opcji switch
#include <iostream>
using namespace std;

int main() {
	cout << "Program sprawdzajacy ktora z liczb jest mniejsza" << endl;
	// z wykorzystaniem switch
	int liczba1;
	int liczba2;
	cout << "Podaj pierwsza liczbe: ";
	cin >> liczba1;
	cout << "Podaj druga liczbe: ";
	cin >> liczba2;
	int wieksza(1);
	if (liczba1 > liczba2) // warunek pozwalajacy zamienic nierownosc na wielkosc calkowita
		wieksza = 0;
	switch (wieksza) {
	case 0:
		cout << "Liczba " << liczba1 << " jest wieksza niz " << liczba2 << endl;
		break;
	case 1:
		cout << "Liczba " << liczba2 << " jest wieksza niz " << liczba1 << endl;
		break;
	}
	return 0;
}
