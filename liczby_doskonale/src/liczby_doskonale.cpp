//============================================================================
// Name        : liczby_doskonale.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
using namespace std;

int main() {
	cout
			<< "Program wypisuje liczby doskonale z przedzialu od 1 do podanej liczby"
			<< endl;
	long long koniec(0);
	while (koniec < 1) {
		cout << "Podaj koniec przedzialu: ";
		cin >> koniec;
		if (koniec < 1)
			cout
					<< "Liczba bedaca koncem przedzialu musi byc wieksza od 1. Sprobuj ponownie"
					<< endl;
		int sumaDzielnikow(0);
		int tymczasowa(1);
		while (tymczasowa <= koniec) {
			for (int i = 1; i <= tymczasowa / 2; ++i) {
				if (tymczasowa % i == 0) {
					sumaDzielnikow += i;
				}
			}
			if (sumaDzielnikow == tymczasowa) {
				cout << tymczasowa << " ";
			}
			++tymczasowa;
			sumaDzielnikow = 0;
		}
	}
	return 0;
}
