//============================================================================
// Name        : wypisanie_liczby_od_konca.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisuje odwrotnosc danej liczby (cyfry zaczynajac od ostatniej" << endl;
	int liczba;
	cout << "Podaj liczbe: ";
	cin >> liczba;
	int tymczasowa(liczba);
	int nowaCyfra;
	while (tymczasowa!=0){
		nowaCyfra = tymczasowa%10;
		cout << nowaCyfra;
		tymczasowa/=10;
	}
	return 0;
}
