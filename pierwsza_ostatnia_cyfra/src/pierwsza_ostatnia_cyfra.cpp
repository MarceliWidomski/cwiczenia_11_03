//============================================================================
// Name        : pierwsza_ostatnia_cyfra.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program wyswietla pierwsza i ostatnia cyfre podanej liczby" << endl;
	int liczba;
	cout << "Podaj liczbe: ";
	cin >> liczba;
	int ostatnia(liczba%10);
	int pierwsza(liczba);
	while(pierwsza/10!=0){
		pierwsza /=10;

	}
	cout << "Pierwsza cyfra to " << pierwsza << " a ostatnia to " << ostatnia << endl;
	return 0;
}
