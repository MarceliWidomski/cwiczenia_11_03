//============================================================================
// Name        : parzysta_switch.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//program sprawdza czy liczba jest parzysta przy uzyciu instrukcji switch
#include <iostream>
using namespace std;

int main() {
	cout << "Program sprawdza czy dana liczba jest parzysta" << endl;
	cout << "Wprowadz liczbe: ";
	int liczba;
	cin >> liczba;
	int czyParzysta(0);
	if ((liczba & 1) == 0) // iloczyn bitowy daje 0 gdy liczba jest parzysta
		czyParzysta = 1;
	switch (czyParzysta) {
	case 1:
		cout << "Liczba jest parzysta" << endl;
		break;
	default:
		cout << "liczba nie jest parzysta" << endl;

	}
	return 0;
}
