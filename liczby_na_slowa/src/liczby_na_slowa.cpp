//============================================================================
// Name        : liczby_na_slowa.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program zapisuje liczby w postaci slow" << endl;
	long liczba;
	cout << "Podaj liczbe: ";
	cin >> liczba;
	if (liczba < 0) { //w przypadku ujemnej wyswietli minus przed pierwszym slowem
		liczba = -liczba;
		cout << "minus ";
	}
	while (liczba > 0) {
		long tymczasowa(liczba);
		unsigned long licznik(1);
		while (tymczasowa / 10 > 0) { // po wyjsciu z petli tymczasowa przedstawia pierwszy znak liczby
			tymczasowa /= 10;
			licznik *= 10;
		}
		switch (tymczasowa) {
		case 1:
			cout << "jeden ";
			break;
		case 2:
			cout << "dwa ";
			break;
		case 3:
			cout << "trzy ";
			break;
		case 4:
			cout << "cztery ";
			break;
		case 5:
			cout << "piec ";
			break;
		case 6:
			cout << "szesc ";
			break;
		case 7:
			cout << "siedem ";
			break;
		case 8:
			cout << "osiem ";
			break;
		case 9:
			cout << "dziewiec ";
			break;
		}
		liczba = liczba - tymczasowa * licznik; // "usuwanie" pierwszego znaku z liczby
	}
	return 0;
}
